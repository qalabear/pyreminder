Python 3.7.1 (v3.7.1:260ec2c36a, Oct 20 2018, 14:05:16) [MSC v.1915 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> from todoist.api import TodoistAPI
>>> apit = TodoistAPI('0123456789abcdef0123456789abcdef01234567')
>>> api = Todoist('0123456789abcdef0123456789abcdef01234567')
Traceback (most recent call last):
  File "<pyshell#2>", line 1, in <module>
    api = Todoist('0123456789abcdef0123456789abcdef01234567')
NameError: name 'Todoist' is not defined
>>> api = TodoistAPI('0123456789abcdef0123456789abcdef01234567')
>>> api.sync()
{'error_tag': 'AUTH_INVALID_TOKEN', 'error_code': 401, 'http_code': 403, 'error_extra': {'retry_after': 2, 'access_type': 'access_token'}, 'error': 'Invalid token'}
>>> api = TodoistAPI('d07729eb4e9cd10b75c57ac794be6e5eff36faa3')
>>> api.sync()
{'full_sync': True, 'temp_id_mapping': {}, 'labels': [], 'locations': [], 'project_notes': [], 'user': {'start_page': 'today', 'features': {'restriction': 3, 'karma_disabled': False, 'karma_vacation': False, 'dateist_lang': None, 'beta': 0, 'auto_invite_disabled': True, 'has_push_reminders': True, 'dateist_inline_disabled': False}, 'completed_today': 0, 'is_premium': True, 'sort_order': 0, 'full_name': 'Matthias', 'auto_reminder': 30, 'id': 18064423, 'share_limit': 26, 'days_off': [6, 7], 'magic_num_reached': True, 'next_week': 1, 'completed_count': 32, 'daily_goal': 5, 'theme': 11, 'tz_info': {'hours': 1, 'timezone': 'Europe/Vienna', 'is_dst': 0, 'minutes': 0, 'gmt_string': '+01:00'}, 'email': 'm.wagener@netconomy.net', 'start_day': 1, 'weekly_goal': 30, 'date_format': 0, 'websocket_url': 'wss://ws.todoist.com/ws?token=cd976f324a172a79', 'inbox_project': 2192903569, 'time_format': 0, 'image_id': None, 'karma_trend': '-', 'business_account_id': None, 'mobile_number': None, 'mobile_host': None, 'premium_until': 'Thu 10 Oct 2019 10:37:46 +0000', 'dateist_lang': None, 'join_date': 'Thu 23 Aug 2018 14:27:17 +0000', 'karma': 1448.0, 'is_biz_admin': False, 'default_reminder': 'push', 'dateist_inline_disabled': False, 'token': 'd07729eb4e9cd10b75c57ac794be6e5eff36faa3'}, 'filters': [{'name': 'Assigned to me', 'color': 12, 'item_order': 1, 'is_favorite': 0, 'query': 'assigned to: me', 'is_deleted': 0, 'id': 2191573842}, {'name': 'Assigned to others', 'color': 12, 'item_order': 2, 'is_favorite': 0, 'query': 'assigned to: others', 'is_deleted': 0, 'id': 2191573843}, {'name': 'Priority 1', 'color': 6, 'item_order': 3, 'is_favorite': 0, 'query': 'priority 1', 'is_deleted': 0, 'id': 2191573844}, {'name': 'Priority 2', 'color': 6, 'item_order': 4, 'is_favorite': 0, 'query': 'priority 2', 'is_deleted': 0, 'id': 2191573845}, {'name': 'Priority 3', 'color': 6, 'item_order': 5, 'is_favorite': 0, 'query': 'priority 3', 'is_deleted': 0, 'id': 2191573846}, {'name': 'Priority 4', 'color': 6, 'item_order': 6, 'is_favorite': 0, 'query': 'priority 4', 'is_deleted': 0, 'id': 2191573847}, {'name': 'View all', 'color': 6, 'item_order': 7, 'is_favorite': 0, 'query': 'view all', 'is_deleted': 0, 'id': 2191573848}, {'name': 'No due date', 'color': 6, 'item_order': 8, 'is_favorite': 0, 'query': 'no date', 'is_deleted': 0, 'id': 2191573849}], 'sync_token': '1wErcz80PYF8aOxAKG6_MjuLl4jF7KAetYbCap-Gl5b4axfnlGfj-Ha0Z7dfiYRS5nJX50QIvye-6J6zo5MFMp2jHQoa4k01VvIN2wyeqFMvanU', 'day_orders': {}, 'projects': [{'color': 7, 'collapsed': 0, 'inbox_project': True, 'is_favorite': 0, 'indent': 1, 'is_deleted': 0, 'id': 2192903569, 'name': 'Inbox', 'has_more_notes': False, 'parent_id': None, 'item_order': 0, 'shared': False, 'is_archived': 0}, {'color': 11, 'collapsed': 0, 'is_favorite': 0, 'indent': 1, 'is_deleted': 0, 'id': 2192903638, 'name': 'OEPAG', 'has_more_notes': False, 'parent_id': None, 'item_order': 0, 'shared': False, 'is_archived': 0}, {'color': 7, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2192904580, 'name': 'Testprozess', 'has_more_notes': False, 'parent_id': 2192903638, 'item_order': 1, 'shared': False, 'is_archived': 0}, {'color': 8, 'collapsed': 0, 'is_favorite': 0, 'indent': 1, 'is_deleted': 0, 'id': 2192903650, 'name': 'QA Lead', 'has_more_notes': False, 'parent_id': None, 'item_order': 2, 'shared': False, 'is_archived': 0}, {'color': 9, 'collapsed': 0, 'is_favorite': 0, 'indent': 1, 'is_deleted': 0, 'id': 2192904111, 'name': 'QMs', 'has_more_notes': False, 'parent_id': None, 'item_order': 3, 'shared': False, 'is_archived': 0}, {'color': 2, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2192907086, 'name': 'Michaela', 'has_more_notes': False, 'parent_id': 2192904111, 'item_order': 4, 'shared': False, 'is_archived': 0}, {'color': 14, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2196268098, 'name': 'Victor', 'has_more_notes': False, 'parent_id': 2192904111, 'item_order': 5, 'shared': False, 'is_archived': 0}, {'color': 19, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2196267995, 'name': 'Paul', 'has_more_notes': False, 'parent_id': 2192904111, 'item_order': 6, 'shared': False, 'is_archived': 0}, {'color': 4, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2196268484, 'name': 'Robert Grillitsch', 'has_more_notes': False, 'parent_id': 2192904111, 'item_order': 7, 'shared': False, 'is_archived': 0}, {'color': 7, 'collapsed': 0, 'is_favorite': 0, 'indent': 2, 'is_deleted': 0, 'id': 2196268363, 'name': 'Andreas', 'has_more_notes': False, 'parent_id': 2192904111, 'item_order': 8, 'shared': False, 'is_archived': 0}], 'collaborators': [], 'day_orders_timestamp': '1344642991.1', 'live_notifications_last_read_id': 2208905005, 'items': [{'day_order': -1, 'assigned_by_uid': None, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Thu 23 Aug 2018 15:21:54 +0000', 'indent': 1, 'date_lang': None, 'id': 2782738553, 'priority': 2, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Teams für Risk Management definieren', 'parent_id': None, 'item_order': 1, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': None, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Thu 23 Aug 2018 15:22:36 +0000', 'indent': 1, 'date_lang': None, 'id': 2782739792, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?', 'parent_id': None, 'item_order': 2, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': None, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Fri 24 Aug 2018 07:32:46 +0000', 'indent': 1, 'date_lang': None, 'id': 2783644475, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Feedback Robert', 'parent_id': None, 'item_order': 3, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': None, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Mon 27 Aug 2018 12:10:27 +0000', 'indent': 1, 'date_lang': None, 'id': 2787173038, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Mini-Präsentation "Internal Beta Testing"', 'parent_id': None, 'item_order': 4, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': None, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Mon 27 Aug 2018 12:11:57 +0000', 'indent': 1, 'date_lang': None, 'id': 2787175856, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Pal: was ist aus Appium geworden?', 'parent_id': None, 'item_order': 5, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 02 Oct 2018 15:11:17 +0000', 'indent': 1, 'date_lang': None, 'id': 2840603421, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Wissensevaluierung für QMs anfertigen', 'parent_id': None, 'item_order': 6, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 02 Oct 2018 15:12:25 +0000', 'indent': 1, 'date_lang': None, 'id': 2840605684, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Jahresziele definieren', 'parent_id': None, 'item_order': 1, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2196268484, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 02 Oct 2018 15:13:06 +0000', 'indent': 1, 'date_lang': None, 'id': 2840606947, 'priority': 4, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing', 'parent_id': None, 'item_order': 7, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 02 Oct 2018 15:14:29 +0000', 'indent': 1, 'date_lang': None, 'id': 2840609397, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Ticket für Jira Custom Field Implementierung', 'parent_id': None, 'item_order': 8, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Mon 08 Oct 2018 11:00:18 +0000', 'indent': 1, 'date_lang': None, 'id': 2848766717, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Recheck Login Consent problem', 'parent_id': None, 'item_order': 3, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903638, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 09 Oct 2018 08:42:11 +0000', 'indent': 1, 'date_lang': None, 'id': 2850466982, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Abklärung mti Sebastian: novas in Graz', 'parent_id': None, 'item_order': 4, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903638, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 09 Oct 2018 09:01:52 +0000', 'indent': 1, 'date_lang': None, 'id': 2850495431, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Robert Riedl für 28h registieren', 'parent_id': None, 'item_order': 9, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 09 Oct 2018 15:03:48 +0000', 'indent': 1, 'date_lang': None, 'id': 2851072840, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Mögliche Aufgaben überlegen', 'parent_id': None, 'item_order': 1, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192907086, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 23 Oct 2018 07:01:23 +0000', 'indent': 1, 'date_lang': None, 'id': 2871806371, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h', 'parent_id': None, 'item_order': 1, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2196268363, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Tue 23 Oct 2018 10:10:19 +0000', 'indent': 1, 'date_lang': None, 'id': 2872062383, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Andreas Gassser auf 25h rauf', 'parent_id': None, 'item_order': 10, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': True, 'in_history': 0, 'date_added': 'Thu 25 Oct 2018 13:09:48 +0000', 'indent': 1, 'date_lang': 'de', 'id': 2875830729, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': 'Thu 25 Oct 2018 21:59:59 +0000', 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste', 'parent_id': None, 'item_order': 11, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': 'Okt 25'}, {'day_order': -1, 'assigned_by_uid': 18064423, 'is_archived': 0, 'labels': [], 'sync_id': None, 'date_completed': None, 'all_day': False, 'in_history': 0, 'date_added': 'Thu 25 Oct 2018 13:21:59 +0000', 'indent': 1, 'date_lang': None, 'id': 2875851115, 'priority': 1, 'checked': 0, 'user_id': 18064423, 'has_more_notes': False, 'due_date_utc': None, 'content': 'Robert Riedl: JF umlegen, nicht remote', 'parent_id': None, 'item_order': 12, 'is_deleted': 0, 'responsible_uid': None, 'project_id': 2192903650, 'collapsed': 0, 'date_string': None}], 'notes': [{'reactions': None, 'is_deleted': 0, 'is_archived': 0, 'file_attachment': None, 'content': 'MIt Team abgeklärt > ginge in Ordnung', 'posted_uid': 18064423, 'uids_to_notify': None, 'item_id': 2850466982, 'project_id': 2192903638, 'id': 2339477005, 'posted': 'Thu 11 Oct 2018 10:12:06 +0000'}, {'reactions': None, 'is_deleted': 0, 'is_archived': 0, 'file_attachment': None, 'content': 'm0 hat das abgenickt', 'posted_uid': 18064423, 'uids_to_notify': None, 'item_id': 2850466982, 'project_id': 2192903638, 'id': 2339477151, 'posted': 'Thu 11 Oct 2018 10:12:18 +0000'}, {'reactions': None, 'is_deleted': 0, 'is_archived': 0, 'file_attachment': None, 'content': 'mit Wuk muss jetzt die Reise organisiert werden, Zeitraum: 5.-8.11.', 'posted_uid': 18064423, 'uids_to_notify': None, 'item_id': 2850466982, 'project_id': 2192903638, 'id': 2339477387, 'posted': 'Thu 11 Oct 2018 10:12:47 +0000'}, {'reactions': None, 'is_deleted': 0, 'is_archived': 0, 'file_attachment': None, 'content': 'Mail von Wuk wurde ausgeschickt mit Informationen', 'posted_uid': 18064423, 'uids_to_notify': None, 'item_id': 2850466982, 'project_id': 2192903638, 'id': 2343878076, 'posted': 'Mon 22 Oct 2018 07:31:48 +0000'}, {'reactions': None, 'is_deleted': 0, 'is_archived': 0, 'file_attachment': None, 'content': 'Wuk hat die Informationen; solll das jetzt handlen.', 'posted_uid': 18064423, 'uids_to_notify': None, 'item_id': 2850466982, 'project_id': 2192903638, 'id': 2344649072, 'posted': 'Tue 23 Oct 2018 10:09:01 +0000'}], 'reminders': [], 'user_settings': {'reminder_push': True, 'reminder_sms': True, 'reminder_desktop': True, 'reminder_email': True}, 'live_notifications': [{'created': 1535478300, 'is_deleted': 0, 'notification_key': 'karma_level_1', 'notification_type': 'karma_level', 'promo_img': 'https://d3ptyyxy2at9ui.cloudfront.net/d3c52f082ccbdf395f0970301f82492e.png', 'karma_level': 1, 'id': 2208905005, 'is_unread': 0}], 'collaborator_states': []}
>>> api.state['projects']
[Project({'collapsed': 0,
 'color': 7,
 'has_more_notes': False,
 'id': 2192903569,
 'inbox_project': True,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 0,
 'name': 'Inbox',
 'parent_id': None,
 'shared': False}), Project({'collapsed': 0,
 'color': 11,
 'has_more_notes': False,
 'id': 2192903638,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 0,
 'name': 'OEPAG',
 'parent_id': None,
 'shared': False}), Project({'collapsed': 0,
 'color': 7,
 'has_more_notes': False,
 'id': 2192904580,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 1,
 'name': 'Testprozess',
 'parent_id': 2192903638,
 'shared': False}), Project({'collapsed': 0,
 'color': 8,
 'has_more_notes': False,
 'id': 2192903650,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 2,
 'name': 'QA Lead',
 'parent_id': None,
 'shared': False}), Project({'collapsed': 0,
 'color': 9,
 'has_more_notes': False,
 'id': 2192904111,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 3,
 'name': 'QMs',
 'parent_id': None,
 'shared': False}), Project({'collapsed': 0,
 'color': 2,
 'has_more_notes': False,
 'id': 2192907086,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 4,
 'name': 'Michaela',
 'parent_id': 2192904111,
 'shared': False}), Project({'collapsed': 0,
 'color': 14,
 'has_more_notes': False,
 'id': 2196268098,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 5,
 'name': 'Victor',
 'parent_id': 2192904111,
 'shared': False}), Project({'collapsed': 0,
 'color': 19,
 'has_more_notes': False,
 'id': 2196267995,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 6,
 'name': 'Paul',
 'parent_id': 2192904111,
 'shared': False}), Project({'collapsed': 0,
 'color': 4,
 'has_more_notes': False,
 'id': 2196268484,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 7,
 'name': 'Robert Grillitsch',
 'parent_id': 2192904111,
 'shared': False}), Project({'collapsed': 0,
 'color': 7,
 'has_more_notes': False,
 'id': 2196268363,
 'indent': 2,
 'is_archived': 0,
 'is_deleted': 0,
 'is_favorite': 0,
 'item_order': 8,
 'name': 'Andreas',
 'parent_id': 2192904111,
 'shared': False})]
>>> api.state['tasks']
Traceback (most recent call last):
  File "<pyshell#8>", line 1, in <module>
    api.state['tasks']
KeyError: 'tasks'
>>> api.state['items']
[Item({'all_day': False,
 'assigned_by_uid': None,
 'checked': 0,
 'collapsed': 0,
 'content': 'Teams für Risk Management definieren',
 'date_added': 'Thu 23 Aug 2018 15:21:54 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2782738553,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 1,
 'labels': [],
 'parent_id': None,
 'priority': 2,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': None,
 'checked': 0,
 'collapsed': 0,
 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?',
 'date_added': 'Thu 23 Aug 2018 15:22:36 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2782739792,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 2,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': None,
 'checked': 0,
 'collapsed': 0,
 'content': 'Feedback Robert',
 'date_added': 'Fri 24 Aug 2018 07:32:46 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2783644475,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 3,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': None,
 'checked': 0,
 'collapsed': 0,
 'content': 'Mini-Präsentation "Internal Beta Testing"',
 'date_added': 'Mon 27 Aug 2018 12:10:27 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2787173038,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 4,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': None,
 'checked': 0,
 'collapsed': 0,
 'content': 'Pal: was ist aus Appium geworden?',
 'date_added': 'Mon 27 Aug 2018 12:11:57 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2787175856,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 5,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Wissensevaluierung für QMs anfertigen',
 'date_added': 'Tue 02 Oct 2018 15:11:17 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2840603421,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 6,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Jahresziele definieren',
 'date_added': 'Tue 02 Oct 2018 15:12:25 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2840605684,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 1,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2196268484,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing',
 'date_added': 'Tue 02 Oct 2018 15:13:06 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2840606947,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 7,
 'labels': [],
 'parent_id': None,
 'priority': 4,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Ticket für Jira Custom Field Implementierung',
 'date_added': 'Tue 02 Oct 2018 15:14:29 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2840609397,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 8,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Recheck Login Consent problem',
 'date_added': 'Mon 08 Oct 2018 11:00:18 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2848766717,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 3,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903638,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Abklärung mti Sebastian: novas in Graz',
 'date_added': 'Tue 09 Oct 2018 08:42:11 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2850466982,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 4,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903638,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Robert Riedl für 28h registieren',
 'date_added': 'Tue 09 Oct 2018 09:01:52 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2850495431,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 9,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Mögliche Aufgaben überlegen',
 'date_added': 'Tue 09 Oct 2018 15:03:48 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2851072840,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 1,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192907086,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h',
 'date_added': 'Tue 23 Oct 2018 07:01:23 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2871806371,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 1,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2196268363,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Andreas Gassser auf 25h rauf',
 'date_added': 'Tue 23 Oct 2018 10:10:19 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2872062383,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 10,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': True,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste',
 'date_added': 'Thu 25 Oct 2018 13:09:48 +0000',
 'date_completed': None,
 'date_lang': 'de',
 'date_string': 'Okt 25',
 'day_order': -1,
 'due_date_utc': 'Thu 25 Oct 2018 21:59:59 +0000',
 'has_more_notes': False,
 'id': 2875830729,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 11,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423}), Item({'all_day': False,
 'assigned_by_uid': 18064423,
 'checked': 0,
 'collapsed': 0,
 'content': 'Robert Riedl: JF umlegen, nicht remote',
 'date_added': 'Thu 25 Oct 2018 13:21:59 +0000',
 'date_completed': None,
 'date_lang': None,
 'date_string': None,
 'day_order': -1,
 'due_date_utc': None,
 'has_more_notes': False,
 'id': 2875851115,
 'in_history': 0,
 'indent': 1,
 'is_archived': 0,
 'is_deleted': 0,
 'item_order': 12,
 'labels': [],
 'parent_id': None,
 'priority': 1,
 'project_id': 2192903650,
 'responsible_uid': None,
 'sync_id': None,
 'user_id': 18064423})]
>>> import requests
requests.get(
    "https://beta.todoist.com/API/v8/tasks",
    params={
        "project_id": 123
    },
    headers={
        "Authorization": "Bearer %s" % your_token
    }).json()

SyntaxError: multiple statements found while compiling a single statement
>>> import requests
>>> requests.get("https://beta.todoist.com/API/v8/tasks", headers={"Authorization": "Bearer %s" % "d07729eb4e9cd10b75c57ac794be6e5eff36faa3"}).json()
[{'id': 2848766717, 'project_id': 2192903638, 'content': 'Recheck Login Consent problem', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2848766717'}, {'id': 2850466982, 'project_id': 2192903638, 'content': 'Abklärung mti Sebastian: novas in Graz', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 5, 'url': 'https://todoist.com/showTask?id=2850466982'}, {'id': 2782738553, 'project_id': 2192903650, 'content': 'Teams für Risk Management definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 2, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782738553'}, {'id': 2782739792, 'project_id': 2192903650, 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?', 'completed': False, 'label_ids': [], 'order': 2, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782739792'}, {'id': 2783644475, 'project_id': 2192903650, 'content': 'Feedback Robert', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2783644475'}, {'id': 2787173038, 'project_id': 2192903650, 'content': 'Mini-Präsentation "Internal Beta Testing"', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787173038'}, {'id': 2787175856, 'project_id': 2192903650, 'content': 'Pal: was ist aus Appium geworden?', 'completed': False, 'label_ids': [], 'order': 5, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787175856'}, {'id': 2840603421, 'project_id': 2192903650, 'content': 'Wissensevaluierung für QMs anfertigen', 'completed': False, 'label_ids': [], 'order': 6, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840603421'}, {'id': 2840606947, 'project_id': 2192903650, 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing', 'completed': False, 'label_ids': [], 'order': 7, 'indent': 1, 'priority': 4, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840606947'}, {'id': 2840609397, 'project_id': 2192903650, 'content': 'Ticket für Jira Custom Field Implementierung', 'completed': False, 'label_ids': [], 'order': 8, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840609397'}, {'id': 2850495431, 'project_id': 2192903650, 'content': 'Robert Riedl für 28h registieren', 'completed': False, 'label_ids': [], 'order': 9, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2850495431'}, {'id': 2872062383, 'project_id': 2192903650, 'content': 'Andreas Gassser auf 25h rauf', 'completed': False, 'label_ids': [], 'order': 10, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2872062383'}, {'id': 2875830729, 'project_id': 2192903650, 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste', 'completed': False, 'label_ids': [], 'order': 11, 'indent': 1, 'priority': 1, 'comment_count': 0, 'due': {'recurring': False, 'string': 'Okt 25', 'date': '2018-10-25'}, 'url': 'https://todoist.com/showTask?id=2875830729'}, {'id': 2875851115, 'project_id': 2192903650, 'content': 'Robert Riedl: JF umlegen, nicht remote', 'completed': False, 'label_ids': [], 'order': 12, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2875851115'}, {'id': 2851072840, 'project_id': 2192907086, 'content': 'Mögliche Aufgaben überlegen', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2851072840'}, {'id': 2871806371, 'project_id': 2196268363, 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2871806371'}, {'id': 2840605684, 'project_id': 2196268484, 'content': 'Jahresziele definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840605684'}]
>>> data = requests.get("https://beta.todoist.com/API/v8/tasks", headers={"Authorization": "Bearer %s" % "d07729eb4e9cd10b75c57ac794be6e5eff36faa3"}).json()
>>> print data
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(data)?
>>> print(data)
[{'id': 2848766717, 'project_id': 2192903638, 'content': 'Recheck Login Consent problem', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2848766717'}, {'id': 2850466982, 'project_id': 2192903638, 'content': 'Abklärung mti Sebastian: novas in Graz', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 5, 'url': 'https://todoist.com/showTask?id=2850466982'}, {'id': 2782738553, 'project_id': 2192903650, 'content': 'Teams für Risk Management definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 2, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782738553'}, {'id': 2782739792, 'project_id': 2192903650, 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?', 'completed': False, 'label_ids': [], 'order': 2, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782739792'}, {'id': 2783644475, 'project_id': 2192903650, 'content': 'Feedback Robert', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2783644475'}, {'id': 2787173038, 'project_id': 2192903650, 'content': 'Mini-Präsentation "Internal Beta Testing"', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787173038'}, {'id': 2787175856, 'project_id': 2192903650, 'content': 'Pal: was ist aus Appium geworden?', 'completed': False, 'label_ids': [], 'order': 5, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787175856'}, {'id': 2840603421, 'project_id': 2192903650, 'content': 'Wissensevaluierung für QMs anfertigen', 'completed': False, 'label_ids': [], 'order': 6, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840603421'}, {'id': 2840606947, 'project_id': 2192903650, 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing', 'completed': False, 'label_ids': [], 'order': 7, 'indent': 1, 'priority': 4, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840606947'}, {'id': 2840609397, 'project_id': 2192903650, 'content': 'Ticket für Jira Custom Field Implementierung', 'completed': False, 'label_ids': [], 'order': 8, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840609397'}, {'id': 2850495431, 'project_id': 2192903650, 'content': 'Robert Riedl für 28h registieren', 'completed': False, 'label_ids': [], 'order': 9, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2850495431'}, {'id': 2872062383, 'project_id': 2192903650, 'content': 'Andreas Gassser auf 25h rauf', 'completed': False, 'label_ids': [], 'order': 10, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2872062383'}, {'id': 2875830729, 'project_id': 2192903650, 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste', 'completed': False, 'label_ids': [], 'order': 11, 'indent': 1, 'priority': 1, 'comment_count': 0, 'due': {'recurring': False, 'string': 'Okt 25', 'date': '2018-10-25'}, 'url': 'https://todoist.com/showTask?id=2875830729'}, {'id': 2875851115, 'project_id': 2192903650, 'content': 'Robert Riedl: JF umlegen, nicht remote', 'completed': False, 'label_ids': [], 'order': 12, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2875851115'}, {'id': 2851072840, 'project_id': 2192907086, 'content': 'Mögliche Aufgaben überlegen', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2851072840'}, {'id': 2871806371, 'project_id': 2196268363, 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2871806371'}, {'id': 2840605684, 'project_id': 2196268484, 'content': 'Jahresziele definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840605684'}]
>>> for item in data: print(data['id'])
KeyboardInterrupt
>>> for task in data['id'] :
	print(task);

	
Traceback (most recent call last):
  File "<pyshell#20>", line 1, in <module>
    for task in data['id'] :
TypeError: list indices must be integers or slices, not str
>>> import json
>>> parsed = json.loads(data)
Traceback (most recent call last):
  File "<pyshell#22>", line 1, in <module>
    parsed = json.loads(data)
  File "C:\Users\Besitzer\AppData\Local\Programs\Python\Python37-32\lib\json\__init__.py", line 341, in loads
    raise TypeError(f'the JSON object must be str, bytes or bytearray, '
TypeError: the JSON object must be str, bytes or bytearray, not list
>>> print(data)
		    
[{'id': 2848766717, 'project_id': 2192903638, 'content': 'Recheck Login Consent problem', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2848766717'}, {'id': 2850466982, 'project_id': 2192903638, 'content': 'Abklärung mti Sebastian: novas in Graz', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 5, 'url': 'https://todoist.com/showTask?id=2850466982'}, {'id': 2782738553, 'project_id': 2192903650, 'content': 'Teams für Risk Management definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 2, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782738553'}, {'id': 2782739792, 'project_id': 2192903650, 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?', 'completed': False, 'label_ids': [], 'order': 2, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782739792'}, {'id': 2783644475, 'project_id': 2192903650, 'content': 'Feedback Robert', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2783644475'}, {'id': 2787173038, 'project_id': 2192903650, 'content': 'Mini-Präsentation "Internal Beta Testing"', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787173038'}, {'id': 2787175856, 'project_id': 2192903650, 'content': 'Pal: was ist aus Appium geworden?', 'completed': False, 'label_ids': [], 'order': 5, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787175856'}, {'id': 2840603421, 'project_id': 2192903650, 'content': 'Wissensevaluierung für QMs anfertigen', 'completed': False, 'label_ids': [], 'order': 6, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840603421'}, {'id': 2840606947, 'project_id': 2192903650, 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing', 'completed': False, 'label_ids': [], 'order': 7, 'indent': 1, 'priority': 4, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840606947'}, {'id': 2840609397, 'project_id': 2192903650, 'content': 'Ticket für Jira Custom Field Implementierung', 'completed': False, 'label_ids': [], 'order': 8, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840609397'}, {'id': 2850495431, 'project_id': 2192903650, 'content': 'Robert Riedl für 28h registieren', 'completed': False, 'label_ids': [], 'order': 9, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2850495431'}, {'id': 2872062383, 'project_id': 2192903650, 'content': 'Andreas Gassser auf 25h rauf', 'completed': False, 'label_ids': [], 'order': 10, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2872062383'}, {'id': 2875830729, 'project_id': 2192903650, 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste', 'completed': False, 'label_ids': [], 'order': 11, 'indent': 1, 'priority': 1, 'comment_count': 0, 'due': {'recurring': False, 'string': 'Okt 25', 'date': '2018-10-25'}, 'url': 'https://todoist.com/showTask?id=2875830729'}, {'id': 2875851115, 'project_id': 2192903650, 'content': 'Robert Riedl: JF umlegen, nicht remote', 'completed': False, 'label_ids': [], 'order': 12, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2875851115'}, {'id': 2851072840, 'project_id': 2192907086, 'content': 'Mögliche Aufgaben überlegen', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2851072840'}, {'id': 2871806371, 'project_id': 2196268363, 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2871806371'}, {'id': 2840605684, 'project_id': 2196268484, 'content': 'Jahresziele definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840605684'}]
>>> parse(json.loads(data))
		    
Traceback (most recent call last):
  File "<pyshell#25>", line 1, in <module>
    parse(json.loads(data))
NameError: name 'parse' is not defined
>>> parsed = json.loads(data)
		    
Traceback (most recent call last):
  File "<pyshell#26>", line 1, in <module>
    parsed = json.loads(data)
  File "C:\Users\Besitzer\AppData\Local\Programs\Python\Python37-32\lib\json\__init__.py", line 341, in loads
    raise TypeError(f'the JSON object must be str, bytes or bytearray, '
TypeError: the JSON object must be str, bytes or bytearray, not list
>>> print(data.type)
		    
Traceback (most recent call last):
  File "<pyshell#27>", line 1, in <module>
    print(data.type)
AttributeError: 'list' object has no attribute 'type'
>>> from pprint import pprint
		    
>>> pprint(data)
		    
[{'comment_count': 0,
  'completed': False,
  'content': 'Recheck Login Consent problem',
  'id': 2848766717,
  'indent': 1,
  'label_ids': [],
  'order': 3,
  'priority': 1,
  'project_id': 2192903638,
  'url': 'https://todoist.com/showTask?id=2848766717'},
 {'comment_count': 5,
  'completed': False,
  'content': 'Abklärung mti Sebastian: novas in Graz',
  'id': 2850466982,
  'indent': 1,
  'label_ids': [],
  'order': 4,
  'priority': 1,
  'project_id': 2192903638,
  'url': 'https://todoist.com/showTask?id=2850466982'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Teams für Risk Management definieren',
  'id': 2782738553,
  'indent': 1,
  'label_ids': [],
  'order': 1,
  'priority': 2,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2782738553'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?',
  'id': 2782739792,
  'indent': 1,
  'label_ids': [],
  'order': 2,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2782739792'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Feedback Robert',
  'id': 2783644475,
  'indent': 1,
  'label_ids': [],
  'order': 3,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2783644475'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Mini-Präsentation "Internal Beta Testing"',
  'id': 2787173038,
  'indent': 1,
  'label_ids': [],
  'order': 4,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2787173038'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Pal: was ist aus Appium geworden?',
  'id': 2787175856,
  'indent': 1,
  'label_ids': [],
  'order': 5,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2787175856'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Wissensevaluierung für QMs anfertigen',
  'id': 2840603421,
  'indent': 1,
  'label_ids': [],
  'order': 6,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2840603421'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Der m0-Termin: Gehalt und Internal Beta Testing',
  'id': 2840606947,
  'indent': 1,
  'label_ids': [],
  'order': 7,
  'priority': 4,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2840606947'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Ticket für Jira Custom Field Implementierung',
  'id': 2840609397,
  'indent': 1,
  'label_ids': [],
  'order': 8,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2840609397'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Robert Riedl für 28h registieren',
  'id': 2850495431,
  'indent': 1,
  'label_ids': [],
  'order': 9,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2850495431'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Andreas Gassser auf 25h rauf',
  'id': 2872062383,
  'indent': 1,
  'label_ids': [],
  'order': 10,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2872062383'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste',
  'due': {'date': '2018-10-25', 'recurring': False, 'string': 'Okt 25'},
  'id': 2875830729,
  'indent': 1,
  'label_ids': [],
  'order': 11,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2875830729'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Robert Riedl: JF umlegen, nicht remote',
  'id': 2875851115,
  'indent': 1,
  'label_ids': [],
  'order': 12,
  'priority': 1,
  'project_id': 2192903650,
  'url': 'https://todoist.com/showTask?id=2875851115'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Mögliche Aufgaben überlegen',
  'id': 2851072840,
  'indent': 1,
  'label_ids': [],
  'order': 1,
  'priority': 1,
  'project_id': 2192907086,
  'url': 'https://todoist.com/showTask?id=2851072840'},
 {'comment_count': 0,
  'completed': False,
  'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h',
  'id': 2871806371,
  'indent': 1,
  'label_ids': [],
  'order': 1,
  'priority': 1,
  'project_id': 2196268363,
  'url': 'https://todoist.com/showTask?id=2871806371'},
 {'comment_count': 0,
  'completed': False,
  'content': 'Jahresziele definieren',
  'id': 2840605684,
  'indent': 1,
  'label_ids': [],
  'order': 1,
  'priority': 1,
  'project_id': 2196268484,
  'url': 'https://todoist.com/showTask?id=2840605684'}]
>>> for x in range(len(data)):
		    print a[x]
		    
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(a[x])?
>>> for x in range(len(data)):
		    print(data[x])

		    
{'id': 2848766717, 'project_id': 2192903638, 'content': 'Recheck Login Consent problem', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2848766717'}
{'id': 2850466982, 'project_id': 2192903638, 'content': 'Abklärung mti Sebastian: novas in Graz', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 5, 'url': 'https://todoist.com/showTask?id=2850466982'}
{'id': 2782738553, 'project_id': 2192903650, 'content': 'Teams für Risk Management definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 2, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782738553'}
{'id': 2782739792, 'project_id': 2192903650, 'content': 'Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?', 'completed': False, 'label_ids': [], 'order': 2, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2782739792'}
{'id': 2783644475, 'project_id': 2192903650, 'content': 'Feedback Robert', 'completed': False, 'label_ids': [], 'order': 3, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2783644475'}
{'id': 2787173038, 'project_id': 2192903650, 'content': 'Mini-Präsentation "Internal Beta Testing"', 'completed': False, 'label_ids': [], 'order': 4, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787173038'}
{'id': 2787175856, 'project_id': 2192903650, 'content': 'Pal: was ist aus Appium geworden?', 'completed': False, 'label_ids': [], 'order': 5, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2787175856'}
{'id': 2840603421, 'project_id': 2192903650, 'content': 'Wissensevaluierung für QMs anfertigen', 'completed': False, 'label_ids': [], 'order': 6, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840603421'}
{'id': 2840606947, 'project_id': 2192903650, 'content': 'Der m0-Termin: Gehalt und Internal Beta Testing', 'completed': False, 'label_ids': [], 'order': 7, 'indent': 1, 'priority': 4, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840606947'}
{'id': 2840609397, 'project_id': 2192903650, 'content': 'Ticket für Jira Custom Field Implementierung', 'completed': False, 'label_ids': [], 'order': 8, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840609397'}
{'id': 2850495431, 'project_id': 2192903650, 'content': 'Robert Riedl für 28h registieren', 'completed': False, 'label_ids': [], 'order': 9, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2850495431'}
{'id': 2872062383, 'project_id': 2192903650, 'content': 'Andreas Gassser auf 25h rauf', 'completed': False, 'label_ids': [], 'order': 10, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2872062383'}
{'id': 2875830729, 'project_id': 2192903650, 'content': 'Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste', 'completed': False, 'label_ids': [], 'order': 11, 'indent': 1, 'priority': 1, 'comment_count': 0, 'due': {'recurring': False, 'string': 'Okt 25', 'date': '2018-10-25'}, 'url': 'https://todoist.com/showTask?id=2875830729'}
{'id': 2875851115, 'project_id': 2192903650, 'content': 'Robert Riedl: JF umlegen, nicht remote', 'completed': False, 'label_ids': [], 'order': 12, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2875851115'}
{'id': 2851072840, 'project_id': 2192907086, 'content': 'Mögliche Aufgaben überlegen', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2851072840'}
{'id': 2871806371, 'project_id': 2196268363, 'content': 'An Katrin: Hochstufung von Andreas Gasser auf 25h', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2871806371'}
{'id': 2840605684, 'project_id': 2196268484, 'content': 'Jahresziele definieren', 'completed': False, 'label_ids': [], 'order': 1, 'indent': 1, 'priority': 1, 'comment_count': 0, 'url': 'https://todoist.com/showTask?id=2840605684'}
>>> for x in range(len(data)):
		    print(data[x]['content'])

		    
Recheck Login Consent problem
Abklärung mti Sebastian: novas in Graz
Teams für Risk Management definieren
Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?
Feedback Robert
Mini-Präsentation "Internal Beta Testing"
Pal: was ist aus Appium geworden?
Wissensevaluierung für QMs anfertigen
Der m0-Termin: Gehalt und Internal Beta Testing
Ticket für Jira Custom Field Implementierung
Robert Riedl für 28h registieren
Andreas Gassser auf 25h rauf
Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste
Robert Riedl: JF umlegen, nicht remote
Mögliche Aufgaben überlegen
An Katrin: Hochstufung von Andreas Gasser auf 25h
Jahresziele definieren
>>> for x in range(len(data)):
		    print(data[x]['content'] + data[x]['due'])

		    
Traceback (most recent call last):
  File "<pyshell#40>", line 2, in <module>
    print(data[x]['content'] + data[x]['due'])
KeyError: 'due'
>>> for x in range(len(data)):
		    print(data[x]['content'] + data[x]['completed'])

		    
Traceback (most recent call last):
  File "<pyshell#42>", line 2, in <module>
    print(data[x]['content'] + data[x]['completed'])
TypeError: can only concatenate str (not "bool") to str
>>> for x in range(len(data)):
		    print(data[x]['content'] + ' ' + str(data[x]['completed']))

		    
Recheck Login Consent problem False
Abklärung mti Sebastian: novas in Graz False
Teams für Risk Management definieren False
Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting? False
Feedback Robert False
Mini-Präsentation "Internal Beta Testing" False
Pal: was ist aus Appium geworden? False
Wissensevaluierung für QMs anfertigen False
Der m0-Termin: Gehalt und Internal Beta Testing False
Ticket für Jira Custom Field Implementierung False
Robert Riedl für 28h registieren False
Andreas Gassser auf 25h rauf False
Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste False
Robert Riedl: JF umlegen, nicht remote False
Mögliche Aufgaben überlegen False
An Katrin: Hochstufung von Andreas Gasser auf 25h False
Jahresziele definieren False
>>> for x in range(len(data)):
		    print(data[x]['content'] + " -> ")
		    if not data[x]['due']:
		    print("no due data set.")
		    
SyntaxError: expected an indented block
>>> 
		    
>>> for x in range(len(data)):
		    print(data[x]['content'] + ' > ')
		    if not data[x]['due']:
		      print("No due date set")

		    
Recheck Login Consent problem > 
Traceback (most recent call last):
  File "<pyshell#52>", line 3, in <module>
    if not data[x]['due']:
KeyError: 'due'
>>> for x in range(len(data)):
		    print(data[x]['content'] + ' > ')
		    if not 'due' in data[x]:
		      print("No due date set")

		    
Recheck Login Consent problem > 
No due date set
Abklärung mti Sebastian: novas in Graz > 
No due date set
Teams für Risk Management definieren > 
No due date set
Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting? > 
No due date set
Feedback Robert > 
No due date set
Mini-Präsentation "Internal Beta Testing" > 
No due date set
Pal: was ist aus Appium geworden? > 
No due date set
Wissensevaluierung für QMs anfertigen > 
No due date set
Der m0-Termin: Gehalt und Internal Beta Testing > 
No due date set
Ticket für Jira Custom Field Implementierung > 
No due date set
Robert Riedl für 28h registieren > 
No due date set
Andreas Gassser auf 25h rauf > 
No due date set
Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste > 
Robert Riedl: JF umlegen, nicht remote > 
No due date set
Mögliche Aufgaben überlegen > 
No due date set
An Katrin: Hochstufung von Andreas Gasser auf 25h > 
No due date set
Jahresziele definieren > 
No due date set
>>> for x in range(len(data)):
		    if not 'due' in data[x]:
		      print(data[x]['content'] + ": No due date set")
		    else:
		      print(data[x]['content'] + ": " + data[x]['due']['date'])

		    
Recheck Login Consent problem: No due date set
Abklärung mti Sebastian: novas in Graz: No due date set
Teams für Risk Management definieren: No due date set
Martin Ofner: Testing Workshop bei Netcademy - wdIO? Galting?: No due date set
Feedback Robert: No due date set
Mini-Präsentation "Internal Beta Testing": No due date set
Pal: was ist aus Appium geworden?: No due date set
Wissensevaluierung für QMs anfertigen: No due date set
Der m0-Termin: Gehalt und Internal Beta Testing: No due date set
Ticket für Jira Custom Field Implementierung: No due date set
Robert Riedl für 28h registieren: No due date set
Andreas Gassser auf 25h rauf: No due date set
Robert Riedll: m0 -> Mitnahme der 11 Urlaubstage ins nächste: 2018-10-25
Robert Riedl: JF umlegen, nicht remote: No due date set
Mögliche Aufgaben überlegen: No due date set
An Katrin: Hochstufung von Andreas Gasser auf 25h: No due date set
Jahresziele definieren: No due date set
>>> 
